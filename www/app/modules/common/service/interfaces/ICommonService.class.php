<?php

namespace common\service\interfaces;

/**
 * 通用服务接口
 * Interface ICommonService
 * @package common\service\interfaces
 * @author yangjian102621@gmail.com
 */
interface ICommonService {

    /**
     * 添加数据
     * @param array $data
     * @return int
     */
    public function add( $data );

    /**
     * 替换数据
     * @param array $data
     * @return boolean
     */
    public function replace( $data );

    /**
     * 删除指定id的数据
     * @param $id
     * @return boolean
     */
    public function delete( $id );

    /**
     * 删除指定条件的数据
     * @param array|string $conditions
     * @return boolean
     */
    public function deletes( $conditions );

    /**
     * 获取数据列表
     * @param array $conditions
     * @param $fields
     * @param $order
     * @param $limit
     * @param $group
     * @param $having
     * @return mixed
     */
    public function getItems($conditions, $fields, $order, $limit, $group, $having);

    /**
     * 获取单条数据
     * @param $condition
     * @param $fields
     * @param $order
     * @return mixed
     */
    public function getItem($condition, $fields, $order);

    /**
     * 更新一条数据
     * @param $data
     * @param $id
     * @return boolean
     */
    public function update( $data, $id );

    /**
     * 批量更新数据
     * @param $data
     * @param $conditions
     * @return mixed
     */
    public function updates( $data, $conditions );

    /**
     * 获取指定条件的记录总数
     * @param $conditions
     * @return int
     */
    public function count( $conditions );

    /**
     * 增加某一字段的值
     * @param string $field
     * @param int $offset 增量
     * @param int $id
     * @return boolean
     */
    public function increase( $field, $offset, $id );

    /**
     * 批量增加指定字段的值
     * @param string $field
     * @param int $offset 增量
     * @param array|string $conditions
     * @return mixed
     */
    public function batchIncrease( $field, $offset, $conditions );

    /**
     * 减少某一字段的值
     * @param string $field
     * @param int $offset 增量
     * @param int $id
     * @return mixed
     */
    public function reduce( $field, $offset, $id );

    /**
     * 批量减少某一字段的值
     * @param string $field
     * @param int $offset 增量
     * @param array|string $conditions
     * @return mixed
     */
    public function batchReduce( $field, $offset, $conditions );

    /**
     * 更新某一字段的值(快捷方法，一次只能更新一个字段)
     * @param $field
     * @param $value
     * @param $id
     * @return mixed
     */
    public function set($field, $value, $id);

    /**
     * 批量更新某一字段的值
     * @see set()
     * @param $field
     * @param $value
     * @param $conditions
     * @return mixed
     */
    public function sets( $field, $value, $conditions );

    //开启事务
    public function beginTransaction();

    //提交更改
    public function commit();

    //回滚
    public function rollback();

    //判断是否开启了事物
    public function inTransaction();

    /**
     * 获取数据库对象
     * @return \herosphp\db\interfaces\Idb
     */
    public function getDB();

    /**
     * @return IModel
     */
    public function where($where); //设置查询条件

    /**
     * @return IModel
     */
    public function field($fields); //设置查询字段

    /**
     * @return IModel
     */
    public function limit($page, $size);

    /**
     * @return IModel
     */
    public function sort($sort);

    /**
     * @return IModel
     */
    public function group($group);

    /**
     * @return IModel
     */
    public function having($having); //设置分组条件
}